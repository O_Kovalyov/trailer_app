package trailer.com.trailer.mappers;

import java.util.ArrayList;
import java.util.List;

import trailer.com.trailer.database.CinemaDbModel;
import trailer.com.trailer.internet.city_and_cinema_model.Cinema;


public class CinemaMapper implements Mapper<List<Cinema>, List<CinemaDbModel>> {
    @Override
    public List<CinemaDbModel> map(List<Cinema> obj) {
        List<CinemaDbModel> cinemaModelList = new ArrayList<>();
        for (Cinema cinema : obj) {
            CinemaDbModel model = new CinemaDbModel();
            model.setCinemaId(cinema.getCinemaId());
            model.setCityId(cinema.getCityId());
            model.setCinemaName(cinema.getCinemaName());
            model.setAddress(cinema.getAddress());
            model.setPhoneNumber(cinema.getPhoneNumber());
            cinemaModelList.add(model);
        }
        return cinemaModelList;
    }

    @Override
    public List<Cinema> reverseMap(List<CinemaDbModel> obj) {
        List<Cinema> cinemaList = new ArrayList<>();
        for (CinemaDbModel cinema : obj) {
            Cinema model = new Cinema();
            model.setCinemaId(cinema.getCinemaId());
            model.setCityId(cinema.getCityId());
            model.setCinemaName(cinema.getCinemaName());
            model.setAddress(cinema.getAddress());
            model.setPhoneNumber(cinema.getPhoneNumber());
            cinemaList.add(model);
        }
        return cinemaList;
    }
}
