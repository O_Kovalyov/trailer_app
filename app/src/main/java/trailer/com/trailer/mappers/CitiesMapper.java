package trailer.com.trailer.mappers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import trailer.com.trailer.database.CityDbModel;
import trailer.com.trailer.internet.city_and_cinema_model.City;


public class CitiesMapper implements Mapper<List<City>,List<CityDbModel>>{

    @Override
    public List<CityDbModel> map(List<City> obj) {
        List<CityDbModel> cityModelList = new ArrayList<>();
        for (City city : obj) {
            CityDbModel model = new CityDbModel();
            model.setCityId(city.getId());
            model.setCityName(city.getName());
            model.setUpdateTime(System.currentTimeMillis());
            cityModelList.add(model);
        }
        return cityModelList;
    }

    public List<City> reverseMap(List<CityDbModel> obj){
        List<City> cityList = new ArrayList<>();
        for (CityDbModel city: obj){
            City model = new City();
            model.setId(city.getCityId());
            model.setName(city.getCityName());
            cityList.add(model);
        }
        return cityList;
    }
}
