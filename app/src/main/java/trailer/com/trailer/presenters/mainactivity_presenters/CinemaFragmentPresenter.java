package trailer.com.trailer.presenters.mainactivity_presenters;

import java.util.ArrayList;
import java.util.List;

import trailer.com.trailer.database.CinemaDbModel;
import trailer.com.trailer.database.DBManipulator;

public class CinemaFragmentPresenter implements CinemaFragmentPresenterInterface {

    public CinemaFragmentPresenter() {
    }

    @Override
    public List<CinemaDbModel> getMainActivityData(int key) {
        return DBManipulator.getInstance().getData(CinemaDbModel.class, "cityId", key);
    }

    public int getCinemaId(int position, int key) {
        return DBManipulator.getInstance().getData(CinemaDbModel.class, "cityId", key).get(position).getCinemaId();
    }
}