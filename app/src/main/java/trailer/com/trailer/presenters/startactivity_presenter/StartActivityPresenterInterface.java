package trailer.com.trailer.presenters.startactivity_presenter;

import android.view.View;

import java.util.List;

public interface StartActivityPresenterInterface {

    void generateApiCall(boolean isConnected);

    List<String> getStartActivityNameData();

    int getStartActivityIdData(int position);

    int onItemClick(View view, int position);
}