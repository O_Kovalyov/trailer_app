package trailer.com.trailer.presenters.cinema_activity_presenter;


import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

public class MapPresenter {
    private  Context context;
    LocationManager locationManager;
    LocationListener locationListener;

    public MapPresenter(Context context) {
        this.context = context;
    }

    public LatLng getPosition(String cinemaAddress){

        LatLng latLng;

        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };
        Geocoder geocoder = new Geocoder(context);
        try {
            List<Address> addresses = geocoder.getFromLocationName(cinemaAddress, 1);
            latLng = new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude());

            return latLng;
        }catch (IndexOutOfBoundsException e){
            String[] parts = cinemaAddress.split(" \\(");
            cinemaAddress = parts[0];
            List<Address> addresses = null;
            try {
                addresses = geocoder.getFromLocationName(cinemaAddress, 1);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            latLng = new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude());
            return latLng;
        } catch (IOException e) {
            e.printStackTrace();
            return new LatLng(0, 0);
        }
    }

    public void centerMapOnLocation(LatLng latLng, GoogleMap mMap, String cinemaName){
        mMap.clear();
        mMap.addMarker(new MarkerOptions().position(latLng).title(cinemaName));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
    }
}
