package trailer.com.trailer.presenters.cinema_activity_presenter;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import trailer.com.trailer.database.CinemaDbModel;
import trailer.com.trailer.database.DBManipulator;
import trailer.com.trailer.database.FilmsDbModel;
import trailer.com.trailer.database.SessionsDBModel;

public class CinemaActivityPresenter implements CinemaActivityPresenterInterface {

    int cinemaId;

    public CinemaActivityPresenter(int cinemaId) {
        this.cinemaId = cinemaId;
    }

    @Override
    public String getCinemaName(int id){
        return DBManipulator.getInstance().getData(CinemaDbModel.class, "cinemaId", id).get(0).getCinemaName();
    }

    @Override
    public String getCinemaAddress(int id){
        return DBManipulator.getInstance().getData(CinemaDbModel.class, "cinemaId", id).get(0).getAddress();
    }

    @Override
    public String getCinemaPhone(int id){
        return DBManipulator.getInstance().getData(CinemaDbModel.class, "cinemaId", id).get(0).getPhoneNumber();
    }

    @Override
    public List<String> getCinemaFilmsOnly(List<FilmsDbModel> filmList){
        List<String> films = new ArrayList<>();
        for(FilmsDbModel model : filmList){
           List<SessionsDBModel> sessionsDBModels = model.getSessionsList();
           for(SessionsDBModel sessions : sessionsDBModels){
            if(sessions.getCinemaId() == cinemaId) {
                films.add(model.getImage());
                break;
            }
           }
        }
        return films;
    }


}
