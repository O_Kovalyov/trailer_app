package trailer.com.trailer.internet.films_and_sessions_model;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

public class Films {

    private String id;
    private String name;
    private String image;
    private String vote;
    private String countries;
    private String actors;
    private String rejisser;
    @SerializedName("sessions")
    private List<Sessions> sessionsList;

    public Films(String id, String name, String image, String vote, String countries, String actors, String rejisser, List<Sessions> sessionsList) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.vote = vote;
        this.countries = countries;
        this.actors = actors;
        this.rejisser = rejisser;
        this.sessionsList = sessionsList;
    }

    public Films() {
        this.id = "";
        this.name = "";
        this.image = "";
        this.vote = "";
        this.countries = "";
        this.actors = "";
        this.rejisser = "";
        this.sessionsList = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getVote() {
        return vote;
    }

    public void setVote(String vote) {
        this.vote = vote;
    }

    public String getCountries() {
        return countries;
    }

    public void setCountries(String countries) {
        this.countries = countries;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public String getRejisser() {
        return rejisser;
    }

    public void setRejisser(String rejisser) {
        this.rejisser = rejisser;
    }

    public List<Sessions> getSessionsList() {
        return sessionsList;
    }

    public void setSessionsList(List<Sessions> sessionsList) {
        this.sessionsList = sessionsList;
    }

    @Override
    public String toString() {
        return "Films{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", image='" + image + '\'' +
                ", vote='" + vote + '\'' +
                ", countries='" + countries + '\'' +
                ", actors='" + actors + '\'' +
                ", rejisser='" + rejisser + '\'' +
                ", sessionsList=" + sessionsList.toString() +
                '}';
    }
}
