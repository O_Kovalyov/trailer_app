package trailer.com.trailer.internet.films_and_sessions_model;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class FilmsAndSessionsModel {

    @SerializedName("result")
    private List<Films> films;

    public List<Films> getFilms() {
        List<Films> filmList = new ArrayList<>();
        for (Films value : films) {
            Films films = new Films(value.getId(), value.getName(), value.getImage(), value.getVote(), value.getCountries(), value.getActors(), value.getRejisser(), value.getSessionsList());
            filmList.add(films);
        }
        return filmList;
    }


    @Override
    public String toString() {
        return "FilmsAndSessionsModel{" +
                "films=" + films +
                '}';
    }
}
