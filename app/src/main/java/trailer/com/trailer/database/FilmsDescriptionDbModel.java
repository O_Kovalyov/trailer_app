package trailer.com.trailer.database;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;


public class FilmsDescriptionDbModel extends RealmObject {

    private String title;
    private String description;
    private RealmList<String> images;
    private RealmList<String> trailers;

    public FilmsDescriptionDbModel(String title, String description,RealmList<String> images, RealmList<String> trailers) {
        this.title = title;
        this.description = description;
        this.images = images;
        this.trailers = trailers;
    }

    public FilmsDescriptionDbModel() {
        title = "";
        description = "";
        images = new RealmList<>();
        trailers = new RealmList<>();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public RealmList<String> getImages() {
        return images;
    }

    public void setImages(RealmList<String> images) {
        this.images = images;
    }

    public RealmList<String> getTrailers() {
        return trailers;
    }

    public void setTrailers(RealmList<String> trailers) {
        this.trailers = trailers;
    }

    @Override
    public String toString() {
        return "FilmsDescription{" +
                "title='" + title + '\'' +
                ", description=" + description +
                ", images=" + images +
                ", trailers=" + trailers +
                '}';
    }
}
