package trailer.com.trailer.database;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import trailer.com.trailer.internet.films_and_sessions_model.Sessions;


public class FilmsDbModel extends RealmObject {

    private String id;
    private String name;
    private String image;
    private String vote;
    private String countries;
    private String actors;
    private String rejisser;
    private RealmList<SessionsDBModel> sessionsList;


    public FilmsDbModel(String id, String name, String image, String vote, String countries, String actors, String rejisser, RealmList<SessionsDBModel> sessionsList) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.vote = vote;
        this.countries = countries;
        this.actors = actors;
        this.rejisser = rejisser;
        this.sessionsList = sessionsList;
    }


    public FilmsDbModel() {
        this.id = "";
        this.name = "";
        this.image = "";
        this.vote = "";
        this.countries = "";
        this.actors = "";
        this.rejisser = "";
        this.sessionsList = new RealmList<>();

    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getVote() {
        return vote;
    }

    public void setVote(String vote) {
        this.vote = vote;
    }

    public String getCountries() {
        return countries;
    }

    public void setCountries(String countries) {
        this.countries = countries;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public String getRejisser() {
        return rejisser;
    }

    public void setRejisser(String rejisser) {
        this.rejisser = rejisser;
    }

    public RealmList<SessionsDBModel> getSessionsList() {
        return sessionsList;
    }
    public void setSessionsList(RealmList<SessionsDBModel> sessionsList) {
        this.sessionsList = sessionsList;
    }


    @Override
    public String toString() {
        return "FilmsDbModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", image='" + image + '\'' +
                ", vote='" + vote + '\'' +
                ", countries='" + countries + '\'' +
                ", actors='" + actors + '\'' +
                ", rejisser='" + rejisser + '\'' +
                ", sessions='" + sessionsList.toString() + '\'' +
                '}';
    }
}
