package trailer.com.trailer.database;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

public class DBManipulator {

    private Realm realm;

    private static DBManipulator INSTANCE;

    private DBManipulator() {
        realm = Realm.getDefaultInstance();
    }

    public static DBManipulator getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DBManipulator();
        }
        return INSTANCE;
    }

    public <T extends RealmObject> void saveData(final List<T> objectList) {
        realm.beginTransaction();
        realm.insert(objectList);
        realm.commitTransaction();
    }

    public <T extends RealmObject> List<T> getData(Class<T> fileClass) {
        List<T> dataList = new ArrayList<>();
        RealmResults<T> realmQuery = realm.where(fileClass)
                .findAll();
        dataList.addAll(realmQuery);
        return dataList;
    }

    public <T extends RealmObject> List<T> getData(Class<T> fileClass, String field, int key) {
        List<T> dataList = new ArrayList<>();
        RealmResults<T> realmQuery = realm.where(fileClass)
                .equalTo(field, key)
                .findAll();
        dataList.addAll(realmQuery);
        return dataList;
    }

    public <T extends RealmObject> List<T> getData(Class<T> fileClass, String field, String key) {
        List<T> dataList = new ArrayList<>();
        RealmResults<T> realmQuery = realm.where(fileClass)
                .equalTo(field, key)
                .findAll();
        dataList.addAll(realmQuery);
        return dataList;
    }

    public void deleteData(){
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();
    }

    public <T extends RealmObject> boolean hasData(Class<T> fileClass) {
        return !realm.where(fileClass).findAll().isEmpty();
    }

    public void onRealmClose() {
        realm.close();
    }
}