package trailer.com.trailer.database;

import io.realm.RealmObject;


public class CityDbModel extends RealmObject {


    private int cityId;
    private String cityName;
    private long updateTime;

    public CityDbModel() {
        cityId = 0;
        cityName = "";
    }

    public CityDbModel(int cityId, String cityName) {
        this.cityId = cityId;
        this.cityName = cityName;
    }

    public void setCityId(final int id) {
        cityId = id;
    }

    public void setCityName(final String name) {
        cityName = name;
    }

    public int getCityId() {
        return cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public String toString() {
        return cityId + " " + cityName;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }
}
