package trailer.com.trailer.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import trailer.com.trailer.R;


public class CityRecyclerViewAdapter extends RecyclerView.Adapter<CityRecyclerViewAdapter.MyViewHolder> {

    private List<String> cityNameDataSet = new ArrayList<>();
    LayoutInflater inflater;

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView tv;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv = itemView.findViewById(R.id.recyclerItem);
        }
    }

    public CityRecyclerViewAdapter(Context context){
        inflater = LayoutInflater.from(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.city_recyclerview_row, parent, false);
        MyViewHolder holder = new MyViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tv.setText(cityNameDataSet.get(position));
    }

    @Override
    public int getItemCount() {
        return cityNameDataSet.size();
    }

    public void addData(List<String> cityNameList){
        this.cityNameDataSet = cityNameList;
        notifyDataSetChanged();
    }

}
