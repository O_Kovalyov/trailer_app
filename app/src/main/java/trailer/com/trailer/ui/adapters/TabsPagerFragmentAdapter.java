package trailer.com.trailer.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class TabsPagerFragmentAdapter extends FragmentPagerAdapter {
    private List<String> tabsList = new ArrayList<>();
    private List<Fragment> listFragments = new ArrayList<>();

    public TabsPagerFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return listFragments.get(position);
    }
    @Override
    public int getCount() {
        return listFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabsList.get(position);
    }

    public void addFragment(Fragment fragment, String title){
        listFragments.add(fragment);
        tabsList.add(title);

    }

}
