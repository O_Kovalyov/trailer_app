package trailer.com.trailer.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import trailer.com.trailer.R;
import trailer.com.trailer.presenters.mainactivity_presenters.CinemaFragmentPresenter;
import trailer.com.trailer.ui.CinemaActivity;
import trailer.com.trailer.ui.adapters.CinemaRecyclerViewAdapter;
import trailer.com.trailer.ui.listener.RecyclerItemClickListener;

public class CinemasFragment extends Fragment{

    private final static int LAYOUT = R.layout.cinemas_fragment_layout;

    private View view;
    private RecyclerView cinemasRecyclerView;
    CinemaRecyclerViewAdapter cinemaRecyclerViewAdapter;
    CinemaFragmentPresenter presenter;
    private int intentMassage;

    public void getIntentMassage(int intentMassage){
        this.intentMassage = intentMassage;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(LAYOUT, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        cinemasRecyclerView = view.findViewById(R.id.cinemasRecyclerView);
        cinemasRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        cinemaRecyclerViewAdapter = new CinemaRecyclerViewAdapter(getContext());
        cinemasRecyclerView.setAdapter(cinemaRecyclerViewAdapter);
        cinemasRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), cinemasRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Intent intent = new Intent(getContext(), CinemaActivity.class);
                int cinemaId = presenter.getCinemaId(position, intentMassage);
                intent.putExtra("id", cinemaId);
                Log.d("Tag", cinemaId + "");
                startActivity(intent);
            }

            @Override
            public void onLongItemClick(View v, int position) {

            }
        }));
        presenter = new CinemaFragmentPresenter();
        cinemaRecyclerViewAdapter.addData(presenter.getMainActivityData(intentMassage));
    }
}

